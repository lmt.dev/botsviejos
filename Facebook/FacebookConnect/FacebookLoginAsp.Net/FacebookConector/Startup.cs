﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FacebookConector.Startup))]
namespace FacebookConector
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
