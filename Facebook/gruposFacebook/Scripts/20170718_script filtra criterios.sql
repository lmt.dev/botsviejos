select nombre
from criterio
where 1 = 1
and ultimaEjecucion > DATE_ADD(current_timestamp(), INTERVAL -1 DAY)
order by ultimaEjecucion ASC
limit 1
;

