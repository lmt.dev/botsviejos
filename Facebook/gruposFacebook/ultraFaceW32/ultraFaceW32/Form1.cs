using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Threading;

namespace ultraFaceW32
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int vProgreso = 0;
        string vPuerto = "";
        string linea1 = "";
        string linea2 = @"echo y | start /B plink -batch -N -L 4444:127.0.0.1:4444 ramon@192.168.2.105 -pw valdomero";
        string linea3 = @"start /B java -jar selenium-server-standalone-3.4.0.jar -role node -nodeConfig node1Config.json";
        

        private void btnIniciarCampa�a_Click(object sender, EventArgs e)
        {
            IniciarCampa�a();
        }

        public void IniciarCampa�a()
        {
            try
            {
                progressBar1.Value = 0;
                if (webBrowser1.Document.Body.InnerText == "201707")
                {
                    StreamReader sr = new StreamReader("c:/Program Files/Corfix/readme.txt");
                    while (sr.Peek() >= 0)
                    {
                        if (sr.ReadLine() == "Compiled")
                        {
                            SetStatus("Directorio verificado...");
                            gbxProgreso.Text = "Progreso actual: 40%";
                            progressBar1.Value = 40;
                            vProgreso = 40;
                            timer1.Start();
                        }
                        else
                        {
                            IniciarConfiguracion();
                        }
                    }
                }
                else
                {
                    SetStatus("Versi�n obsoleta. Obtener nueva version en www.corfixtecnologia.com ");
                }

            }
            catch (Exception ex)
            {
                IniciarConfiguracion();
            }
        }

        public void ExecuteCommand(string _Command)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + _Command);
                // Indicamos que la salida del proceso se redireccione en un Stream
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
                procStartInfo.CreateNoWindow = true;
                //Inicializa el proceso
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
            }
            catch(Exception ex)
            {
                SetStatus("Error en ejecucion de comando: " + ex.Message);
            }
        }

        public void IniciarConfiguracion()
        {
            try
            {
                CrearPath();
                DescargarArchivo("chromedriver.exe");
                DescargarArchivo("node1Config.json");
                DescargarArchivo("plink.exe");
                DescargarArchivo("selenium-server-standalone-3.4.0.jar");
                StreamWriter sw = new StreamWriter("c:/Program Files/Corfix/readme.txt");
                sw.Write("Compiled");
                sw.Close();
            }
            catch (Exception ex)
            {
                SetStatus("Error en descarga de archivos de configuracion: " + ex.Message);
            }
        }
        private void EscribirArchivo(string pPath, string pContenido)
        {
            SumarProgreso();
            StreamWriter sw = new StreamWriter(pPath);
            sw.WriteLine(pContenido);
            sw.Close();
        } 

        public void DescargarArchivo(string pArchivo)
        {
            try
            {
                SetStatus("Ejecutando configuraci�n...");
                SumarProgreso();
                WebClient wc = new WebClient();
                wc.DownloadFile("http://www.corfixtecnologia.com/ultrafaceselenium/" + pArchivo, "C:\\Program Files\\Corfix\\" + pArchivo);
                Thread.Sleep(5000);
                Application.DoEvents();
                GC.Collect();
            }
            catch (Exception ex)
            {
                SetStatus("Error en descarga de archivo: " + ex.Message);
            }
        }

        public void SumarProgreso()
        {
            try
            {
                if (vProgreso < 100 )
                {
                    vProgreso = vProgreso + 10;
                }
                progressBar1.Value = vProgreso;
                gbxProgreso.Text = "Progreso actual: "+vProgreso+" %";
            }
            catch (Exception ex)
            {
                SetStatus("Error en sumatoria de progreso: " + ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("http://corfixtecnologia.com/ultrafaceselenium/htmlVersion.html");
            timerReboot.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int pgb = progressBar1.Value;
            switch(pgb)
            {
                case 40:
                    if (tbxUsuario.Text != "")
                    {
                        if (tbxContrase�a.Text != "")
                        {
                            gbxUsuario.Enabled = false;
                            gbxContrase�a.Enabled = false;
                            btnIniciarCampa�a.Enabled = false;
                            webBrowser1.Navigate("http://kodos-pc:88/fb/getp.php?obtenerpuerto=asd&usuario=" + tbxUsuario.Text + "&contrasena=" + tbxContrase�a.Text);
                            SetStatus("Validando credenciales...");
                            SumarProgreso();
                        }
                        else
                        {
                            FrenarLogin();
                            SetStatus("Contrase�a vac�a");
                        }
                    }
                    else
                    {
                        FrenarLogin();
                        SetStatus("Usuario vac�o");
                    }
                    break;
                
                case 50:
                    vPuerto = webBrowser1.Document.Body.InnerText;
                    SetStatus("Creando conexion al servidor...");
                    SumarProgreso();
                    break;

                case 60:
                    linea1 = @"echo y | start /min """" ""c:/Program Files/Corfix/plink"" -batch -N -R "+vPuerto+":127.0.0.1:5555 ramon@192.168.2.105 -pw valdomero";
                    linea2 = @"echo y | start /min """" ""c:/Program Files/Corfix/plink"" -batch -N -L 4444:127.0.0.1:4444 ramon@192.168.2.105 -pw valdomero";
                    linea3 = @"start /min """" java -Dwebdriver.chrome.driver=""c:/Program Files/Corfix/chromedriver.exe"" -jar ""c:/Program Files/Corfix/selenium-server-standalone-3.4.0.jar"" -role node -nodeConfig ""c:/Program Files/Corfix/node1Config.json""";
                    SetStatus("Preparando entorno local...");
                    SumarProgreso();
                    break;

                case 70:
                    SetStatus("Ejecutando cliente de conexion reversa...");
                    ExecuteCommand(linea1);
                    SumarProgreso();
                    break;

                case 80:
                    SetStatus("Ejecutando cliente de conexion saliente...");
                    ExecuteCommand(linea2);
                    SumarProgreso();
                    break;

                case 90:
                    SetStatus("Ejecutando cliente de automatizacion...");
                    ExecuteCommand(linea3);
                    SumarProgreso();
                    break;

                case 100:
                    SetStatus("Iniciando publicador...");
                    timer1.Stop();
                    timer2.Start();
                   break;
                    
            }
        }

        public void FrenarLogin()
        {
            timer1.Stop();
            progressBar1.Value = 0;
        }

        public void SetStatus(string mensaje)
        {
            lblStatus.Text = mensaje+" "+ DateTime.Now;
        }

        public void CrearPath()
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo("c:/Program Files/Corfix/");
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch(Exception ex)
            {
                SetStatus("Error al crear path de instalaci�n: "+ex.Message);
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            webBrowser1.Navigate("http://kodos-pc:88/fb/getp.php?iniciarpublicador=asd&usuario=" + tbxUsuario.Text + "&contrasena=" + tbxContrase�a.Text + "&puerto=" + vPuerto);
            SetStatus("Publicador iniciado");
            timer2.Stop();
        }

        private void timerReboot_Tick(object sender, EventArgs e)
        {
            IniciarCampa�a();
        }
    }
}