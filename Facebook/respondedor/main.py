#! /usr/bin/python3
# -*- coding: utf-8 -*-
import time
import sys
from time import sleep

from selenium.webdriver.common.keys import Keys

from clases import Driver
from clases import VD
from cuenta import Cuenta
from speech import Speech


def worker():


    remote = False
    virtualid = "53"
    virtualsupport = False
#inicializa objetos
    driver = Driver()
    driver.firefox = True
    driver.profile = "/home/lucas/.mozilla/firefox/mwad0hks.default"
    #driver.chrome = True

#leer argumentos de inicio
    for c, ar in enumerate(sys.argv):
        if ar == "-m" or ar == "--mode":
            if sys.argv[c+1] == "virtual":
                virtualsupport = True
                virtualid = sys.argv[c+2]
            if sys.argv[c+1] == "remoto":
                remoteurl = sys.argv[c+2] #puerto solamente
                remote = True


    try:
        if remote == True:
            driver.remoteurl = remoteurl
            driver.startRemotedriver()

        elif virtualsupport:
            virtualdisplay = VD(virtualid)
            virtualdisplay.start_virtual_display()
            driver.startDriver()
        else:
            driver.startDriver()
    except Exception as e:
        print(e)
    #obtener cuenta harcodeada
    cuenta = Cuenta()
    #instanciar robot respondedor pasandole un web-driver y una cuenta de facebook
    rsp = Respondedor(driver, cuenta)
    #iniciar sesion en facebook
    rsp.iniciarSesion()
    try:
        #leer notificaciones cada 5 segundos
        while True:
            rsp.leerNotificaciones()
            print("Leyendo notificaciones "+time.strftime("%H:%M:%S"))
            sleep(5)

    except Exception as e:
        print(e)
        pass

    #cerrar to al carajo
    rsp.close()
    virtualdisplay.quit()




class Respondedor:
    fbsesion = False
    driver = None
    cuenta = None
    url = None
    def __init__(self, driver, cuenta):
        self.driver = driver
        self.url = "https://www.facebook.com/settings"
        self.cuenta = cuenta
        self.comprobarSesion()
    def leerNotificaciones(self):
        try:
            #obtener contador de notificaciones de facebook
            notifiCounter = int(self.driver.find_element_by_id("notificationsCountValue").text)
        except:
            notifiCounter = 0
        if notifiCounter > 0 :
            #expandir notificaciones
            self.driver.find_element_by_xpath("//div[@id='fbNotificationsJewel']/a/div").click()
            sleep(1)
            #lista de notificaciones [web-element, web-element, web-element]
            notificacioneselements = self.driver.find_elements_by_xpath("//*[@data-testid='notif_list_item']")
            notificaciones = []
            for nelement in notificacioneselements:
                #obtener lista de elementos de cada notificacion
                contenido = nelement.find_elements_by_tag_name('a')
#                sleep(1)
                for c in contenido:
#                    sleep(1)
                    notificacion = {}
                    #guardar texto de la notificacion
                    notificacion["text"] = c.text
                    #guardar url de la notificacion
                    notificacion["url"] = c.get_attribute("href")
                    notificaciones.append(notificacion)

            #itinerar cada notificacion extraida
            for n in notificaciones:
#                sleep(1)
                #solo leer las notificaciones nuevas
                if notifiCounter > 0:
                    try:
                        if not n["text"] == "":
                            notifiCounter = notifiCounter - 1
                            #procesar texto de elemento del loop
                            cadena = str(n["text"])
                            cadena = cadena.replace("hace unos segundos","")
                            if "comentó" in cadena:
                                nombre = cadena.replace("comentó tu","").split("publicación")[0]
                            elif "comentaron" in cadena:
                                nombre = cadena.replace("comentaron tu", "").split("publicación")[0]
                            elif "commented" in cadena:
                                nombre = cadena.replace("commented on your", "").replace(" and ", "y").split("publicación")[0]
                            else:
                                #ignorar notificaciones y continuar el loop en el siguiente elemento
                                continue
                            print("")
                            print("Abriendo comentario")
                            self.driver.goto(n["url"])
                            sleep(1)
                            cc = Speech(nombre)
                            self.driver.find_elements_by_xpath("//a[contains(@class,'UFIReplyLink')]")[-1].click()
                            sleep(3)
                            caja = self.driver.find_element_by_xpath("//div[text()='Escribe una respuesta...']")
                            caja.send_keys(cc)
                            #sleep(2)
                            self.driver.find_element_by_xpath("//*[contains(text(), '"+cc+"')]").send_keys(Keys.ENTER)
                            self.driver.goto(self.url)
                            sleep(2)
                    except Exception as e:
                        print(e)
                        pass
            #self.driver.goto("http://facebook.com")
    def comprobarSesion(self):
        self.tag = "comprobarSesion()"
        try:
            check1 = self.driver.find_elements_by_xpath("//*[contains(text(), 'Email or Phone')]")
            check2 = self.driver.find_elements_by_xpath("//*[contains(text(), 'Correo electrónico o teléfono')]")
            check3 = self.driver.find_elements_by_xpath("//*[contains(text(), 'Iniciar sesión')]")
            if check1 or check2 or check3:

                self.fbsesion = False
            else:

                self.fbsesion = True

        except Exception as e:

            if "'NoneType' object has no attribute 'get'" in str(e):

                sys.exit()
            if "without establishing a connection" in str(e):

                sys.exit()
            if "Connection refused" in str(e):

                sys.exit()
            pass
        return self.fbsesion

    def cerrarSesion(self):
        self.tag = "cerrarSesion()"
        if self.fbsesion:
            try:
                self.driver.find_element_by_id("userNavigationLabel").click()
                sleep(2)
                self.driver.find_element_by_xpath("//*[contains(text(), 'Salir')]").click()
                sleep(1)
            except Exception as e:
                print(e)
                pass

    def iniciarSesion(self):
        self.tag = "iniciarSesion()"
        try:
            self.driver.goto("https://www.facebook.com")
            sleep(3)
            self.driver.find_element_by_xpath("//input[@id='email']").clear()
            sleep(1)
            self.driver.find_element_by_xpath("//input[@id='email']").send_keys(self.cuenta.dispositivo)
            sleep(1)
            self.driver.find_element_by_xpath("//input[@id='pass']").send_keys(self.cuenta.dispositivocontrasena)
            sleep(1)
            try:
                self.driver.find_element_by_xpath("//input[@value='Iniciar sesión']").click()
                sleep(2)
            except Exception as e:
                try:
                    self.driver.find_element_by_xpath("//input[@value='Entrar']").click()
                except Exception as e:

                    self.driver.find_element_by_xpath("//input[@value='Log In']").click()
            sleep(3)
        except Exception as e:
            pass
    def close(self):
        self.driver.quit()



#esto se ejecuta al iniciar/importar el script
if __name__ == '__main__':
  worker()



