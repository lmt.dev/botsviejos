from random import randint


def varianteConector():
    i = randint(0, 3)

    if i == 0:
        result = "por"

    if i == 1:
        result = "via"

    if i == 2:
        result = "mediante"

    if i == 3:
        result = "en"

    return result

def varianteAccion1():
    i = randint(0, 3)

    if i == 0:
        result = "consulta"

    if i == 1:
        result = "pregunta"

    if i == 2:
        result = "duda"

    if i == 3:
        result = "mensaje"

    return result

def varianteAccion2():
    i = randint(0, 2)

    if i == 0:
        result = "espero"

    if i == 1:
        result = "aguardo"

    if i == 2:
        result = "recibiré con gusto"

    return result


def varianteSaludo():
    i = randint(0, 5)
    if i == 0:
        result = ". Saludos!"

    if i == 1:
        result = ". Nos vemos!"

    if i == 2:
        result = ". Exitos!"

    if i == 3:
        result = ". Un abrazo!"

    if i == 4:
        result = ". Le saludo atentamente"

    if i == 5:
        result = ". Que tenga un buen dia"

    return result
	
def varianteMedio():
    i = randint(0, 4)
    if i == 0:
        result = " whatspp 3518126143"
	
    if i == 1:
        result = " nuestro whatsapp 3518126143"
	
    if i == 2:
        result = " email a tvporinternet@corfixtecnologia.com o "+varianteConector()+" whatspp 3518126143"
	
    if i == 3:
        result = " nuestro celular 3518126143. Recibimos Whatspp!"
		
    if i == 4:
     result = " web en www.corfixtecnologia.com o "+varianteConector()+" whatsapp 3518126143"
    return result

def Speech(n):
    i = randint(0,1)
    if i == 0:

        if " y " in n:
            result = n + " "+varianteAccion2()+" sus "+varianteAccion1()+"s "+varianteConector()+varianteMedio()+varianteSaludo()
        else:
            result = n + " "+varianteAccion2()+" su "+varianteAccion1()+" "+varianteConector()+varianteMedio()+varianteSaludo()

    elif i == 1:

        if " y " in n:
            result = varianteAccion2()+" sus "+varianteAccion1()+"s "+varianteConector()+varianteMedio()+". "+n+varianteSaludo()
        else:
            result = varianteAccion2()+" su "+varianteAccion1()+" "+varianteConector()+varianteMedio()+". "+n+varianteSaludo()

    return result

