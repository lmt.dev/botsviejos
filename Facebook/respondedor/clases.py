import os

from pyvirtualdisplay import Display
from selenium import webdriver

firefoxprofile = '/home/kodos/.mozilla/firefox/93tthpza.discador'


class Driver:
    url = None
    profile = None
    driver = None
    virtual = None
    virtualdisplay = None
    remote = None
    remoteurl = None
    profile = firefoxprofile
    driver = None
    firefox = False
    chrome = False

    def startDriver(self):
        if self.firefox:
            self.driver = webdriver.Firefox(webdriver.FirefoxProfile(self.profile))
        if self.chrome:
            self.driver = webdriver.Chrome('chromedriver')
        return self.driver

    def startRemotedriver(self):
        #        self.driver = webdriver.Remote(command_executor='http://'+self.remoteurl+'/wd/hub',desired_capabilities={'browserName': 'chrome',"chromeOptions": {"args":['--user-data-dir=/home/kodos/.config/google-chrome','--profile-directory=Default']}})
        self.driver = webdriver.Remote(command_executor='http://127.0.0.1:' + self.remoteurl + '/wd/hub',
                                       desired_capabilities={'browserName': 'chrome', "chromeOptions": {
                                           "args": ['--user-data-dir=C:\\sel\\profile',
                                                    '--profile-directory=Default']}})
        return self.driver

    def goto(self, u):
        self.driver.set_page_load_timeout(30)
        self.driver.get(u)

    def execute_script(self, s):
        self.driver.execute_script(s)

    def find_elements_by_css_selector(self, ss):
        return self.driver.find_elements_by_css_selector(ss)

    def find_element_by_css_selector(self, e):
        return self.driver.find_element_by_css_selector(e)

    def find_elements_by_xpath(self, sss):
        return self.driver.find_elements_by_xpath(sss)

    def find_element_by_xpath(self, ee):
        return self.driver.find_element_by_xpath(ee)

    def find_element_by_link_text(self, ssss):
        return self.driver.find_element_by_link_text(ssss)

    def find_element_by_id(self, sssss):
        return self.driver.find_element_by_id(sssss)

    def find_elements_by_id(self, ii):
        return self.driver.find_elements_by_id(ii)

    def find_element_by_tag_name(self, t):
        return self.driver.find_element_by_tag_name(t)

    def find_elements_by_tag_name(self, tt):
        return self.driver.find_elements_by_tag_name(tt)

    def find_element_by_name(self, ssssss):
        return self.driver.find_element_by_name(ssssss)

    def find_elements_by_class_name(self, cns):
        return self.driver.find_elements_by_class_name(cns)

    def find_element_by_class_name(self, cn):
        return self.driver.find_element_by_class_name(cn)

    def click(self):
        self.driver.click()

    def quit(self):
        try:
            self.driver.quit()
        except:
            pass


class VD:
    id = None
    display = None

    def __init__(self, i):
        self.id = i

    def print_id(self):
        print(self.id)

    def start_virtual_display(self):
        #        print("iniciando virtual display","start_virtual_display")
        os.environ['DISPLAY'] = ':' + self.id
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()

    def quit(self):
        self.display.stop()


class Mysql:
    username = None
    password = None
    host = None
    db = "gruposface"
    conn = None

    def __init__(self):
        self.username = "root"
        self.password = "C4rp1nt3r00"
        self.host = "127.0.0.1"

    def conectar(self):
        self.conn = pymysql.connect(host=self.host, user=self.username, password=self.password, db=self.db,
                                    charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
        return self.conn

    def desconectar(self):
        self.conn.close()


class Loger:
    logdir = None
    logfile = None
    username = None

    def escribir(self, linea):
        l = linea.replace("'", "").replace("`", "").replace("Ç", "").replace("´", "")
        #self.logdir = "/home/kodos/corfix/vet/TMP/"
        self.hs = open(self.logdir + self.logfile, "a")
        if (os.path.getsize(self.logdir + self.logfile) > 0):
            self.hs.write(l + "," + "\n")
        else:
            self.hs.write(l + "," + "\n")
        self.hs.close()


class Apuesta:
    nombre = None
    original = None
    p1 = None
    p2 = None
    p3 = None
    token = None

    def __init__(self, n, o, c, f1, f2, f3, t):
        self.nombre = n
        self.original = o
        self.casa = c
        self.p1 = f1
        self.p2 = f2
        self.p3 = f3
        self.token = t

    def nombre(self):
        return self.nombre


class Comando:
    token = None
    cmd = None
    partido = None
    casa = None
    usuario = None
    paga = None
    monto = None
    minimo = None
    ganancia = None

    def __init__(self, tkn, cmd, ptd, csa, usr, pga, mnto, mno, gncia):
        self.token = tkn
        self.cmd = cmd
        self.partido = ptd
        self.casa = csa
        self.usuario = usr
        self.paga = pga
        self.monto = mnto
        self.minimo = mno
        self.ganancia = gncia
