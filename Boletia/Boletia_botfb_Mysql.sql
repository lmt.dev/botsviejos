
-- CATEGORIAS
call SP_facebookCategorie_GetSet('prueba verbose name','prueba tag');

select * from facebookCategorie;

-- EVENTOS
CALL `boletia_botfb`.`SP_facebookEvent_GetSet`
(
'pmain_organizer_fbpage_id555', 
'pfbevent_id333', 
current_timestamp(), 
'paddress222', 
'platitude222', 
'plongitude222', 
123, 
1234, 
'plink222', 
'ptickets_at222', 
'pdetails222', 
'pqualified_lead_status222', 
current_timestamp()
);
  /*
  parametros:
	pmain_organizer_fbpage_id varchar(500),
	pfbevent_id varchar(500),
	pevent_date timestamp,
	paddress varchar(500),
	platitude varchar(500),
	plongitude varchar(500),
	pattendees int,
	pinterested int,
	plink varchar(500),
	ptickets_at varchar(500),
	pdetails varchar(500),
	pqualified_lead_status varchar(500),
	plead_qualified_at timestamp
    */
select * from facebookEvent;

-- PAGINAS
CALL SP_facebookPage_GetSet
(
	'<{pfbpage_id varchar(500)}>', 
	'<{pfbpage_title varchar(500)}>', 
	current_timestamp(), -- '<{pfbpage_created_at timestamp}>', 
	current_timestamp(), -- '<{poperations_started_at timestamp}>', 
	'<{pphone varchar(500)}>', 
	'<{pcellphone varchar(500)}>', 
	'<{pwebsite varchar(500)}>', 
	'<{pemail varchar(500)}>', 
	'<{pdescription varchar(500)}>', 
	'<{pranking varchar(500)}>', 
	'<{plikes varchar(500)}>', 
	'<{plink varchar(500)}>', 
	'<{paddress varchar(500)}>', 
	'<{platitude varchar(500)}>', 
	'<{plongitude varchar(500)}>', 
	1, -- '<{pverified int(11)}>', 
	'<{presponse_time varchar(500)}>', 
	current_timestamp(), -- '<{plast_post_date timestamp}>', 
	current_timestamp(), -- '<{pcreated_at timestamp}>', 
	current_timestamp(), -- '<{pscrapping_updated_at timestamp}>', 
	current_timestamp(), -- '<{pupdated_at timestamp}>', 
	'<{pqualified_lead_status varchar(500)}>', 
	current_timestamp() -- '<{plead_qualified_at timestamp}>'
);

select * from facebookPage;

-- -----------------------
-- RELACIONES
-- EVENTO POR PAGINA
CALL `boletia_botfb`.`SP_facebookEvent_facebookPage_Set`
(
	<{pfacebookEventId int}>, 
	<{pfacebookPageId int}>
);

-- PAGINA POR CATEGORIA
CALL SP_facebookPage_facebookCategorie_Set
(
	<{pfacebookPageId int}>, 
	<{pfacebookCategorieId int}>
);