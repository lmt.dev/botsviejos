CREATE DEFINER=`root`@`%` PROCEDURE `SP_facebookEvent_GetSet`(
pmain_organizer_fbpage_id varchar(500),
pfbevent_id varchar(500),
pevent_date timestamp,
paddress varchar(500),
platitude varchar(500),
plongitude varchar(500),
pattendees int,
pinterested int,
plink varchar(500),
ptickets_at varchar(500),
pdetails varchar(500),
pqualified_lead_status varchar(500),
plead_qualified_at timestamp
)
BEGIN
	declare vfacebookEventId int default 0;
    set vfacebookEventId = (select ifnull(facebookEventId,0)from facebookEvent where fbevent_id = pfbevent_id);
	if (vfacebookEventId = 0 or vfacebookEventId = null) then
		INSERT INTO facebookEvent
		(
			main_organizer_fbpage_id,
			fbevent_id,
			event_date,
			address,
			latitude,
			longitude,
			attendees,
			interested,
			link,
			tickets_at,
			details,
			created_at,
			scrapping_updated_at,
			updated_at,
			qualified_lead_status,
			lead_qualified_at
        )
		VALUES
		(
			pmain_organizer_fbpage_id,
			pfbevent_id,
			current_timestamp(),
			paddress,
			platitude,
			plongitude,
			pattendees,
			pinterested,
			plink,
			ptickets_at,
			pdetails,
			current_timestamp(),
			current_timestamp(),
			current_timestamp(),
			pqualified_lead_status,
			current_timestamp()
        );
		commit;
    end if;
    set vfacebookEventId = (select ifnull(facebookEventId,0) from facebookEvent where fbevent_id = pfbevent_id);
    select vfacebookEventId;
END