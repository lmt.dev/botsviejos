from datetime import datetime


def int_timestamp():
    return int(datetime.now().strftime('%Y%m%d%H%M%S'))