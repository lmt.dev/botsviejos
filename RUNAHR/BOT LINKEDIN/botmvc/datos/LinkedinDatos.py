import pymysql

hostName = "127.0.0.1"
userName = "runahr"
userPassword = "8kOaTSG3u7KvrQ0K"
databaseName = "runahr"
#cusrorType = pymysql.cursors.DictCursor


#call PRC_ObtenerCriterioBusqueda();

def ObtenerCriterioBusqueda():
    try:
        databaseConnection = pymysql.connect(host=hostName, user=userName, password=userPassword, db=databaseName,autocommit=True)
        cursorObject = databaseConnection.cursor()
        cursorObject.callproc("PRC_ObtenerCriterioBusqueda")
        for result in cursorObject.fetchall():
            return result[0]
    except Exception as e:
        print("Exeception occured", e)
    finally:
        try:
            databaseConnection.close()
        except:
            pass

def ObtenerCuentaLinkedin():
    try:
        databaseConnection = pymysql.connect(host=hostName, user=userName, password=userPassword, db=databaseName,autocommit=True)
        cursorObject = databaseConnection.cursor()
        cursorObject.callproc("PRC_ObtenerCuentaLinkedin")
        for result in cursorObject.fetchall():
            return result
    except Exception as e:
        print("Exeception occured", e)
    finally:
        try:
            databaseConnection.close()
        except:
            pass


def CargarLeads(bot):
    for lead in bot.entidad.leads:
        pNombreCompania = lead.nombre_ultima_compania
        pWebSite = lead.sitio_compania
        pPrimerNombre = lead.nombre
        pApellido = lead.apellido
        pEmail = lead.email
        pPuesto = lead.puesto
        pCiudad = lead.ciudad
        pPais = lead.pais
        pTelefono = lead.telefono
        pIndustria = lead.industria
        pRango = lead.rango
        pUrlPerfilLinkedin = lead.url
        try:
            databaseConnection = pymysql.connect(host=hostName, user=userName, password=userPassword, db=databaseName, autocommit=True)
            cursorObject = databaseConnection.cursor()
            argumentos = [pNombreCompania,pWebSite,pPrimerNombre,pApellido,pEmail,pPuesto,pCiudad,pPais,pTelefono,pIndustria,pRango,pUrlPerfilLinkedin]
            cursorObject.callproc("PRC_CargarLead", argumentos)
            #for result in cursorObject.fetchall():
            #    print(result)
            print("lead insertado...")
        except Exception as e:
            print("Exeception occured",e)
            print(("PRC_CargarLead", argumentos))
        finally:
            try:
                databaseConnection.close()
            except:
                pass




