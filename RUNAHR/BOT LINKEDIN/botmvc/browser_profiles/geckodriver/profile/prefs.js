// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.startupRolloutPrefs.layout.display-list.retain", true);
user_pref("app.normandy.startupRolloutPrefs.security.tls.version.fallback-limit", 4);
user_pref("app.normandy.user_id", "ebd17082-0c92-4c50-8427-40f3f0127ecd");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1535324589);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1535401637);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1534448149);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1535313627);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1534540049);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 358400);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.migration.version", 68);
user_pref("browser.newtabpage.activity-stream.impressionId", "{45f0ee8b-c7ff-47bb-a790-d91a82da731d}");
user_pref("browser.newtabpage.activity-stream.migrationExpired", true);
user_pref("browser.newtabpage.activity-stream.migrationLastShownDate", 1534474800);
user_pref("browser.newtabpage.activity-stream.migrationRemainingDays", 2);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.onboarding.notification.last-time-of-changing-tour-sec", 1535313622);
user_pref("browser.onboarding.seen-tourset-version", 2);
user_pref("browser.onboarding.tour-type", "new");
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1535401611101");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1535403386101");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1535401612033");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1535405212033");
user_pref("browser.search.countryCode", "AR");
user_pref("browser.search.region", "AR");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20180712150008");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1535167942");
user_pref("browser.startup.homepage_override.buildID", "20180712150008");
user_pref("browser.startup.homepage_override.mstone", "61.0.1");
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":14,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Yahoo");
user_pref("browser.urlbar.timesBeforeHidingSuggestionsHint", 0);
user_pref("devtools.onboarding.telemetry.logged", true);
user_pref("distribution.iniFile.exists.appversion", "61.0.1");
user_pref("distribution.iniFile.exists.value", true);
user_pref("distribution.mint.bookmarksProcessed", true);
user_pref("extensions.blocklist.lastModified", "Mon, 27 Aug 2018 18:17:57 GMT");
user_pref("extensions.blocklist.pingCountTotal", 2);
user_pref("extensions.blocklist.pingCountVersion", 2);
user_pref("extensions.databaseSchema", 26);
user_pref("extensions.getAddons.cache.lastUpdate", 1535324591);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppBuildId", "20180712150008");
user_pref("extensions.lastAppVersion", "61.0.1");
user_pref("extensions.lastPlatformVersion", "61.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webextensions.uuids", "{\"webcompat@mozilla.org\":\"bfb73280-277f-448f-b8da-105a9b4eacd1\",\"screenshots@mozilla.org\":\"bac9c1a6-5554-4df7-b7b7-7e8ddf3905bd\"}");
user_pref("lightweightThemes.usedThemes", "[]");
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1535324596);
user_pref("media.gmp-gmpopenh264.version", "1.7.1");
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.predictor.cleaned-up", true);
user_pref("pdfjs.enabledCache.initialized", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.history.expiration.transient_current_max_pages", 104858);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("privacy.sanitize.pending", "[]");
user_pref("security.sandbox.content.tempDirSuffix", "92280fc1-ebe0-41e8-909d-6a3649742e4f");
user_pref("services.blocklist.addons.checked", 1535401565);
user_pref("services.blocklist.gfx.checked", 1535401565);
user_pref("services.blocklist.onecrl.checked", 1535401565);
user_pref("services.blocklist.pinning.checked", 1535401565);
user_pref("services.blocklist.plugins.checked", 1535401565);
user_pref("services.settings.clock_skew_seconds", 72);
user_pref("services.settings.last_etag", "\"1535393877555\"");
user_pref("services.settings.last_update_seconds", 1535401565);
user_pref("signon.importedFromSqlite", true);
user_pref("toolkit.startup.last_success", 1535401606);
user_pref("toolkit.telemetry.cachedClientID", "6cd2a5cb-76d0-4aa2-a0f6-474e78b6468a");
user_pref("toolkit.telemetry.previousBuildID", "20180712150008");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
