from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("linkedin_boletia", ["linkedin_boletia.py"]),
    Extension("FacebookDatos",  ["datos/FacebookDatos.py"]),
    Extension("LinkedinDatos",  ["datos/LinkedinDatos.py"]),
    Extension("Bot",  ["entidades/Bot.py"]),
    Extension("Facebook",  ["entidades/Facebook.py"]),
    Extension("Linkedin",  ["entidades/Linkedin.py"]),
    Extension("BotNegocio",  ["negocio/BotNegocio.py"]),
    Extension("FacebookNegocio",  ["negocio/FacebookNegocio.py"]),
    Extension("LinkedinNegocio",  ["negocio/LinkedinNegocio.py"])
]

setup(
    name = 'My Program Name',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)

