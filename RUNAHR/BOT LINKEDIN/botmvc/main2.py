import gc

from datos.FacebookDatos import ObtenerCuentaFacebook
from entidades.Bot import Bot
from entidades.Facebook import Facebook
from negocio.BotNegocio import iniciar_navegador
from negocio.FacebookNegocio import iniciar_sesion, buscar_pagina

def get_bot():
    return [o for o in gc.get_objects() if isinstance(o, Bot)][0]

facebook = Facebook()

usuario, contrasena, cantidad_criterios = ObtenerCuentaFacebook()
facebook.usuario = usuario
facebook.contrasena = contrasena

bot = Bot(facebook)
iniciar_navegador(bot)
iniciar_sesion(bot)
buscar_pagina(bot)


