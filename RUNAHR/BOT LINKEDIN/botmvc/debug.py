import gc

from entidades.Bot import *

def get_bot():
    return [o for o in gc.get_objects() if isinstance(o, Bot)][0]


