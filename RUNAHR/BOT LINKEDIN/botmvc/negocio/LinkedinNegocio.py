from time import sleep

from selenium.webdriver.common.keys import Keys

from datos.LinkedinDatos import CargarLeads, ObtenerCriterioBusqueda
from entidades.Linkedin import Lead
from negocio.BotNegocio import esperar_por, scroll_down


def cambiar_pagina(bot):
    if bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_boton_pagina_siguiente):
        print("cambiando de pagina")
        bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_pagina_siguiente).location_once_scrolled_into_view
        esperar_por(bot, bot.entidad.XPATH_boton_pagina_siguiente)
        bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_pagina_siguiente).click()
        sleep(0.5)
        esperar_por(bot, bot.entidad.XPATH_pagiandor_completo)
        return True
    else:
        print("no hay mas paginas")
        return False


def buscar_perfil(bot):
    esperar_por(bot, bot.entidad.XPATH_campo_busqueda)
    print("Iniciando búsqueda por criterio...")
    bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_busqueda).send_keys(ObtenerCriterioBusqueda())
    bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_busqueda).send_keys(Keys.RETURN)

    lista_resultados = []
    var_pagina = 0
    while True:

        lista_meta_resultados = bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_resultado_de_busqueda)
        if len(lista_meta_resultados) < 7:
            continue

        # esperar_por(bot, bot.entidad.XPATH_resultado_de_busqueda, cantidad_minima_elementos=7, scrollear=True)

        for _meta_resultado in lista_meta_resultados:
            _meta_resultado.location_once_scrolled_into_view
            _url = _meta_resultado.find_element_by_xpath(".//a").get_property("href")
            _titulo = _meta_resultado.find_element_by_xpath(bot.entidad.XPATH_titulo_resultado_busqueda).text
            try:
                _detalle = _meta_resultado.find_element_by_xpath(bot.entidad.XPATH_sublinea1_resultado_busqueda).text
            except:
                pass
            _pais = _meta_resultado.find_element_by_xpath(bot.entidad.XPATH_sublinea2_resultado_busqueda).text
            try:
                _puestoempresa = _meta_resultado.find_element_by_xpath(
                    bot.entidad.XPATH_sublinea3_resultado_busqueda).text
            except:
                _puestoempresa = ""

            boton_conectar = _meta_resultado.find_elements_by_xpath(bot.entidad.XPATH_boton_conectar)

            if "Ciudad de México" in _pais and boton_conectar:
                print("resultados de mexico con boton", _titulo)
                print("entrando...")

                _cantidad_ventanas = len(bot.webdriver.window_handles)  # contar tabs/ventanas para no perder control de la principal
                _meta_resultado.find_element_by_xpath(".//a").send_keys(Keys.CONTROL + Keys.RETURN)  # abrir nuevo tab
                sleep(1)
                _nuevo_tab = bot.webdriver.window_handles[-1]
                while not esperar_por(bot, bot.entidad.XPATH_boton_ver_informacion, timeout=1):
                    # sleep(2)#esperando que se abra un nuevo tab/ventana
                    bot.webdriver.switch_to_window(_nuevo_tab)  # foco en nuevo tab
                esperar_por(bot, bot.entidad.XPATH_boton_ver_informacion)  # esperar q cargue la web

                # bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_ver_informacion).click()
                # esperar_por(bot,".//artdeco-modal-content[@class = 'ember-view']")
                # info = bot.webdriver.find_elements_by_xpath(".//artdeco-modal-content[@class = 'ember-view']")[0].text

                try:
                    nombre_ultima_compania = bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_trabajo_actual).text
                except:
                    nombre_ultima_compania = None
                sitio_compania = None
                nombre_perfil = bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_nombre_perfil).text
                apellido_perfil = None
                email_perfil = None
                puesto_perfil = bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_puesto_perfil).text
                ciudad_pais_perfil = bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_ciudad_pais_perfil).text.split(",")
                ciudad_perfil = ','.join(ciudad_pais_perfil[:-1])
                pais_perfil = ciudad_pais_perfil[-1]
                telefono_perfil = None
                industria_perfil = None
                rango_perfil = None
                url_perfil = "https://www.linkedin.com/in/"+bot.webdriver.current_url.split("/")[4]

                obj_lead = Lead(nombre_ultima_compania, sitio_compania, nombre_perfil, apellido_perfil, email_perfil,puesto_perfil, ciudad_perfil, pais_perfil, telefono_perfil, industria_perfil,rango_perfil, url_perfil)

                bot.webdriver.close()
                bot.webdriver.switch_to_window(bot.webdriver.window_handles[0])  # recuperar foco en tab principal

                lista_resultados.append(obj_lead)  # guardar resultados filtrados en lista para return

                bot.entidad.leads = [obj_lead]
                CargarLeads(bot)


        if not cambiar_pagina(bot):
            break
        var_pagina = var_pagina + 1
        print("pagina:", var_pagina)
    bot.entidad.leads = lista_resultados
    #CargarLeads(bot)
    #return lista_resultados


def iniciar_sesion(bot):
    try:
        # verificar idioma
        if bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_campo_correo_login_ing):
            bot.entidad.XPATH_campo_correo_login = bot.entidad.XPATH_campo_correo_login_ing
            bot.entidad.XPATH_campo_contrasena = bot.entidad.XPATH_campo_contrasena_ing
            bot.entidad.XPATH_boton_iniciar_sesion = bot.entidad.XPATH_boton_iniciar_sesion_ing

        print("Verificando sesión...")
        if not bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_control_sesion_iniciada):
            print("Sesión no iniciada... iniciando sesión...")
            esperar_por(bot, bot.entidad.XPATH_campo_correo_login)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_correo_login).send_keys(bot.entidad.usuario)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_contrasena).send_keys(bot.entidad.contrasena)

            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_iniciar_sesion).send_keys(Keys.RETURN)
            sleep(1)
            print("Inicio de sesión exitoso!")
            return True
        else:
            return False


    except Exception as e:
        print("Error al iniciar sesion: ", e)
