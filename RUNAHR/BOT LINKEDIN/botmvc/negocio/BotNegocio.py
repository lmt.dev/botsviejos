import logging
import sys
from time import sleep

from selenium import webdriver
from selenium.webdriver import DesiredCapabilities, FirefoxProfile
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as Firefox_Options
from selenium.webdriver.remote.remote_connection import LOGGER

from funciones import int_timestamp


def esperar_por(bot, xpath, timeout=20, cantidad_minima_elementos=1, scrollear=False):
    resultado = False
    hora_de_inicio = int_timestamp()
    while (int_timestamp() - hora_de_inicio) < timeout:
        try:
            if scrollear:
                scroll_down(bot)
            if len(bot.webdriver.find_elements_by_xpath(xpath)) >= cantidad_minima_elementos:
                resultado = True
                break
        except:
            print("Error...")
            pass
        finally:
            sleep(1)

    if (int_timestamp() - hora_de_inicio) >= timeout:
        print("timeout", xpath)

    return resultado


def iniciar_navegador(bot):
    LOGGER.setLevel(logging.NOTSET)

    os = sys.platform
    if os == "linux":
        bot.navegador_path = "bin/" + bot.navegador
        bot.perfil_path = "browser_profiles/" + bot.navegador + "/" + bot.perfil
    elif os == "win32":
        bot.navegador_path = "bin//" + bot.navegador + ".exe"
        bot.perfil_path = "browser_profiles//" + bot.navegador + "//" + bot.perfil


    firefox_capabilities = DesiredCapabilities.FIREFOX
    firefox_capabilities['marionette'] = True

    firefox_profile = FirefoxProfile(profile_directory=bot.perfil_path)

    if bot.headless:
        options = Firefox_Options()

        options.add_argument("--headless")

        bot.webdriver = webdriver.Firefox(firefox_options=options, executable_path=bot.navegador_path,
                                           firefox_profile=firefox_profile, capabilities=firefox_capabilities)
    else:
        bot.webdriver = webdriver.Firefox(executable_path=bot.navegador_path, firefox_profile=firefox_profile,
                                           capabilities=firefox_capabilities)


    bot.webdriver.get(bot.entidad.url)





def scroll_down(bot):
    bot.webdriver.execute_script("window.scrollTo(0, document.body.scrollHeight);")



