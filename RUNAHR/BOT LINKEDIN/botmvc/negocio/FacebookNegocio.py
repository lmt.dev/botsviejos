from time import sleep

from selenium.webdriver.common.keys import Keys

from datos.FacebookDatos import ObtenerCriterioBusqueda
from entidades.Facebook import Pagina
from funciones import int_timestamp
from negocio.BotNegocio import esperar_por, scroll_down


def iniciar_sesion(bot):
    try:
        print("Verificando sesión...")
        if not bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_control_sesion_iniciada):
            print("Sesión no iniciada... iniciando sesión...")
            esperar_por(bot, bot.entidad.XPATH_campo_correo_login)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_correo_login).clear()
            sleep(0.6)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_correo_login).send_keys(bot.entidad.usuario)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_contrasena).send_keys(bot.entidad.contrasena)
            bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_contrasena).send_keys(Keys.RETURN)
            sleep(1)
            print("Inicio de sesión exitoso!")
            return True
        else:
            return False
    except Exception as e:
        print("Error al iniciar sesion: ", e)


def buscar_pagina(bot):
    esperar_por(bot, bot.entidad.XPATH_campo_busqueda)
    print("Iniciando búsqueda de pagina por criterio...")
    bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_busqueda).send_keys(ObtenerCriterioBusqueda())
    bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_campo_busqueda).send_keys(Keys.RETURN)
    esperar_por(bot,bot.entidad.XPATH_boton_paginas)
    bot.webdriver.find_element_by_xpath(bot.entidad.XPATH_boton_paginas).click()
    esperar_por(bot,bot.entidad.XPATH_lista_contenedores_resultado)

    lista_contenedores_usados = []
    lista_contenedores_usados_cache = []
    rta = []
    run = True
    reintentar = 3
    hora_de_inicio = int_timestamp()
    while run:
        lista_contenedores = bot.webdriver.find_elements_by_xpath(bot.entidad.XPATH_lista_contenedores_resultado)
        if len(lista_contenedores) > len(lista_contenedores_usados):
            reintentar = 3
            for contenedor in lista_contenedores:
                if contenedor in lista_contenedores_usados:
                    continue
                resultados = contenedor.find_elements_by_xpath(bot.entidad.XPATH_resultados) + contenedor.find_elements_by_xpath(bot.entidad.XPATH_resultados_alt)
                if resultados:
                    for resultado in resultados:
                        resultado.location_once_scrolled_into_view
                        url = resultado.find_elements_by_xpath(bot.entidad.XPATH_resultado_contenido1)[0].find_element_by_xpath("./a").get_attribute("href")
                        titulo = resultado.find_elements_by_xpath(bot.entidad.XPATH_resultado_contenido1)[1].find_elements_by_xpath("./div/div")[0].find_elements_by_xpath("./div")[1].find_element_by_xpath(".//span").text
                        linea1 = "".join(resultado.find_elements_by_xpath(bot.entidad.XPATH_resultado_contenido1)[1].find_elements_by_xpath("./div/div")[1].find_elements_by_xpath("./div")[0].text.split("·"))
                        linea2 = resultado.find_elements_by_xpath(bot.entidad.XPATH_resultado_contenido1)[1].find_elements_by_xpath("./div/div")[1].find_elements_by_xpath("./div")[1].text
                        obj_pagina = Pagina(url,titulo,linea1,linea2)
                        #print(url,titulo,linea1,linea2)
                        print(titulo)
                        rta.append(obj_pagina)
                lista_contenedores_usados.append(contenedor)
        else:
            if reintentar > 0:
                scroll_down(bot)
                sleep(1)
                reintentar = reintentar - 1
            else:
                run = False


    print("finzalido en:",int_timestamp() - hora_de_inicio)
    return rta

