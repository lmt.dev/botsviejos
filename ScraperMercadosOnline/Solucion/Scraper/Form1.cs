﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;
using MagicSQL;
using ScraperMercadosOnline;

namespace Scraper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            iniciarBotOLX();
        }
        
        //OLX
        int indicePagina = 1;
        int cantidadCache = 0;
        string pLink = "https://www.olx.com.ar/motocicletas-scooters-cat-379-p-";
        bool detener = false;

        bool recorrerDetallesAvisoIndividual = false;

        protected void iniciarBotOLX()
        {
            try
            {
                string url = "";
                ChromeOptions chromeOptions = new ChromeOptions();
                ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
                ChromeDriver driver = null;

                chromeOptions.AddArguments(new List<string>() { "headless" });
                driver = new ChromeDriver(chromeDriverService, chromeOptions);
                
                List<Aviso> listaAvisosOLXInsertar = new List<Aviso>();

                while (detener == false)
                {
                    Aviso avisoRecorrerDetalles = new Aviso().Select().Where(avi => 
                        avi.Celular == null &&
                        avi.Destacado == true
                        ).FirstOrDefault();

                    if (avisoRecorrerDetalles == null)
                    {
                        avisoRecorrerDetalles = new Aviso().Select().Where(avi =>
                        avi.Celular == null
                        ).FirstOrDefault();
                    }

                    if (avisoRecorrerDetalles != null)
                    {
                        recorrerDetallesAvisoIndividual = true;
                    }
                    else
                    {
                        recorrerDetallesAvisoIndividual = false;
                    }

                    if (recorrerDetallesAvisoIndividual == false)
                    {
                        //Buscar avisos

                        Fuente fuenteConsultar = new Fuente().Select().FirstOrDefault();
                        indicePagina = Convert.ToInt32(fuenteConsultar.MaximoPaginas) + 1;
                        url = pLink + indicePagina.ToString();

                        System.Diagnostics.Debug.WriteLine("Iniciando procesamiento. Pagina " + pLink + " Indice " + indicePagina.ToString());
                        driver.Navigate().GoToUrl(url);
                        try
                        {
                            IWebElement listaCruda = driver.FindElement(By.ClassName("items-list "));

                            IList<IWebElement> listaProcesada = listaCruda.FindElements(By.TagName("li"));
                            listaAvisosOLXInsertar.Clear();
                            foreach (IWebElement avisoRecorrido in listaProcesada)
                            {
                                Aviso avisoOLX = new Aviso();

                                IList<IWebElement> listaBuscarImagen = avisoRecorrido.FindElements(By.ClassName("items-image"));
                                IWebElement avisoSector1BuscarImagen = listaBuscarImagen[0];
                                string inputSector1BuscarImagen = avisoSector1BuscarImagen.GetAttribute("outerHTML");
                                string regexBuscarImagen = "img src=\"(.*)\"";
                                Match matchBuscarImagen = Regex.Match(inputSector1BuscarImagen, regexBuscarImagen);
                                if (matchBuscarImagen.Success)
                                {
                                    string stringBuscarImagen = matchBuscarImagen.Groups[1].Value;
                                    string[] avisoUrlFiltrado1 = Regex.Split(stringBuscarImagen, "alt=");
                                    avisoOLX.ImagenUrl = avisoUrlFiltrado1[0];
                                    avisoOLX.ImagenUrl = avisoOLX.ImagenUrl.Replace("\"", "");
                                }

                                IList<IWebElement> listaSectoresAviso = avisoRecorrido.FindElements(By.TagName("div"));
                                IWebElement avisoSector1 = listaSectoresAviso[0];
                                string[] arrayAvisoOtrosAtributos = Regex.Split(avisoSector1.Text, "\\r");
                                IWebElement avisoSector2 = listaSectoresAviso[1];
                                string inputSector1 = avisoSector1.GetAttribute("outerHTML");

                                bool esPesoArgentino = arrayAvisoOtrosAtributos[1].Contains("$");
                                bool esDolar = arrayAvisoOtrosAtributos[1].Contains("USD");


                                if (esPesoArgentino)
                                {
                                    string[] avisoLugarPrecio = arrayAvisoOtrosAtributos[1].Split('$');
                                    avisoOLX.Lugar = avisoLugarPrecio[0];
                                    string[] PrecioFechaPublicacion = avisoLugarPrecio[1].Split(' ');
                                    avisoOLX.Precio = PrecioFechaPublicacion[0];

                                    avisoOLX.Moneda = "ARS";
                                }

                                if (esDolar)
                                {
                                    string[] avisoLugarPrecio = Regex.Split(arrayAvisoOtrosAtributos[1], "USD");
                                    avisoOLX.Lugar = avisoLugarPrecio[0];
                                    string[] PrecioFechaPublicacion = avisoLugarPrecio[1].Split(' ');
                                    avisoOLX.Precio = PrecioFechaPublicacion[0];

                                    avisoOLX.Moneda = "USD";
                                }

                                if (!esPesoArgentino && !esDolar)
                                {
                                    avisoOLX.Lugar = arrayAvisoOtrosAtributos[1];
                                }

                                if (avisoOLX.Lugar.Contains("Hoy"))
                                {
                                    avisoOLX.Lugar = avisoOLX.Lugar.Replace("Hoy", "");
                                }

                                int indiceBuscarDestacado = 3;
                                if (arrayAvisoOtrosAtributos[2].Contains("Negociable"))
                                {
                                    avisoOLX.Negociable = true;
                                }
                                else
                                {
                                    indiceBuscarDestacado = indiceBuscarDestacado - 1;
                                }

                                avisoOLX.FechaPublicacion = arrayAvisoOtrosAtributos[indiceBuscarDestacado];
                                if (avisoOLX.FechaPublicacion.Contains("DESTACADO"))
                                {
                                    avisoOLX.FechaPublicacion = null;
                                    avisoOLX.Destacado = true;
                                }

                                if (!arrayAvisoOtrosAtributos[(indiceBuscarDestacado)].Contains("Contactar"))
                                {
                                    if (arrayAvisoOtrosAtributos[(indiceBuscarDestacado + 1)].Contains("DESTACADO"))
                                    {
                                        avisoOLX.Destacado = true;
                                    }
                                }

                                string regex = "href=\"(.*)\"";
                                Match match = Regex.Match(inputSector1, regex);
                                if (match.Success)
                                {
                                    string avisoUrl = match.Groups[1].Value;
                                    string[] avisoUrlFiltrado = Regex.Split(avisoUrl, "data-qa");
                                    string avisoUrlFiltradoSplit = avisoUrlFiltrado[0]
                                                                    .Replace("\"", "")
                                                                    .Replace("//", "")
                                                                    .Replace(" ", "");

                                    Aviso avisoConsulta = new Aviso().Select().Where(avi =>
                                        avi.Url == avisoUrlFiltradoSplit
                                        ).FirstOrDefault();

                                    if (avisoConsulta == null)
                                    {

                                        avisoOLX.Titulo = avisoUrlFiltrado[1].Replace("\"", "");
                                        avisoOLX.Titulo = avisoOLX.Titulo.Replace("=list-item title=", "");
                                        avisoOLX.FHAlta = DateTime.Now;
                                        avisoOLX.Url = avisoUrlFiltradoSplit;
                                        listaAvisosOLXInsertar.Add(avisoOLX);
                                        System.Diagnostics.Debug.WriteLine("Link agregado: " + avisoUrlFiltradoSplit);
                                    }
                                }
                            }
                            indicePagina++;
                            System.Diagnostics.Debug.WriteLine("Indice sumarizado: " + indicePagina.ToString());
                            using (Tn tn = new Tn("ScraperMercadosOnline"))
                            {
                                foreach (Aviso avisoAgregar in listaAvisosOLXInsertar)
                                {
                                    avisoAgregar.Insert(tn);
                                }

                                Fuente fuenteActualizarPaginas =
                                    new Fuente().Select().FirstOrDefault();
                                fuenteActualizarPaginas.MaximoPaginas = fuenteActualizarPaginas.MaximoPaginas + 1;
                                fuenteActualizarPaginas.Update(tn);

                                tn.Commit();
                                System.Diagnostics.Debug.WriteLine("Registros insertados: " + listaAvisosOLXInsertar.Count.ToString());
                                Thread.Sleep(5000);
                                tn.Dispose();
                            }

                        }
                        catch (Exception ex)
                        {
                            //detener = true;
                            System.Diagnostics.Debug.WriteLine("Problemas para impactar la transaccion de los registros. Error: " + ex.Message);
                            indicePagina = indicePagina + 1;
                            using (Tn tn2 = new Tn("ScraperMercadosOnline"))
                            {
                                foreach (Aviso avisoSalvar in listaAvisosOLXInsertar)
                                {
                                    avisoSalvar.Insert(tn2);
                                }

                                Fuente fuenteActualizarPaginas =
                                    new Fuente().Select().Where(f =>
                                    f.Url == pLink
                                        ).FirstOrDefault();
                                fuenteActualizarPaginas.MaximoPaginas = fuenteActualizarPaginas.MaximoPaginas + 1;
                                fuenteActualizarPaginas.Update(tn2);

                                tn2.Commit();
                                System.Diagnostics.Debug.WriteLine(listaAvisosOLXInsertar.Count.ToString() + " avisos salvados");
                            }
                        }
                    }
                    else
                    {
                        //Recorrer detalles de aviso individual
                        url = avisoRecorrerDetalles.Url;
                        System.Diagnostics.Debug.WriteLine("Iniciando procesamiento. Aviso individual " + avisoRecorrerDetalles.IdAviso.ToString());
                        driver.Navigate().GoToUrl("https://"+url);

                        bool contieneCelular = false;
                        try
                        {
                            //Revelar numero
                            driver.FindElement(By.ClassName("reveal")).Click();
                            contieneCelular = true;
                        }
                        catch { }

                        IWebElement avisoIndividualContenido = driver.FindElement(By.Id("content"));
                        string[] contenidoSpliteado = Regex.Split(avisoIndividualContenido.Text, "\\r");                           
                            
                        foreach (string encontrarConcesionaria in contenidoSpliteado)
                        {
                            if (encontrarConcesionaria.Contains("Concesionaria"))
                            {
                                avisoRecorrerDetalles.EsConcesionaria = true;
                            }
                        }

                        if (contieneCelular)
                        {
                            int indiceCelular = 0;
                            foreach (string itemContenido in contenidoSpliteado)
                            {
                                if (itemContenido.Contains("+54"))
                                {
                                    avisoRecorrerDetalles.Celular = itemContenido.Replace("+549", "").Replace("+54", "");
                                }
                                indiceCelular++;
                            }
                        }
                        else
                        {
                            avisoRecorrerDetalles.Celular = "No detectado";
                        }

                        avisoRecorrerDetalles.Update();

                        System.Diagnostics.Debug.WriteLine("Detalles de aviso actualizado. ID Aviso: " + avisoRecorrerDetalles.IdAviso.ToString());
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {                
               System.Diagnostics.Debug.WriteLine("Error intentando iniciar selenium: " + ex.Message);
            }

        }


    }
}
