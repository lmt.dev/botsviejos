﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;
using MagicSQL;

namespace ScraperMercadosOnline
{
    class Scraper
    {
        static void Main(string[] args)
        {
            
        }

        protected void iniciarBotOLX()
        {
            try
            {
                chromeOptions.AddArguments(new List<string>() { "headless" });
                chromeOptions.AddArguments(new List<string>() { "headless" });
                driver = new ChromeDriver(chromeDriverService, chromeOptions);

                string url = pLink + indicePagina.ToString();
                System.Diagnostics.Debug.WriteLine("Iniciando procesamiento. Pagina " + pLink + " Indice " + indicePagina.ToString());

                while (detener == false)
                {
                    url = pLink + indicePagina.ToString();
                    driver.Navigate().GoToUrl(url);
                    try
                    {
                        IWebElement listaCruda = driver.FindElement(By.ClassName("items-list "));
                        IList<IWebElement> listaProcesada = listaCruda.FindElements(By.TagName("li"));
                        foreach (IWebElement avisoRecorrido in listaProcesada)
                        {
                            IList<IWebElement> itemAviso = avisoRecorrido.FindElements(By.TagName("div"));
                            IWebElement itemAvisoParte1 = itemAviso[0];
                            string input = itemAvisoParte1.GetAttribute("outerHTML");
                            string regex = "href=\"(.*)\"";
                            Match match = Regex.Match(input, regex);
                            if (match.Success)
                            {
                                string avisoUrl = match.Groups[1].Value;
                                string[] avisoUrlFiltrado = Regex.Split(avisoUrl, "data-qa");
                                string avisoUrlFiltradoSplit = avisoUrlFiltrado[0].Replace("\"", "").Replace("//", "").Replace(" ", "");

                                Aviso avisoConsulta = new Aviso().Select().Where(avi => avi.Url == avisoUrlFiltradoSplit).FirstOrDefault();



                                if (avisoConsulta == null)
                                {
                                    Aviso avisoOLX = new Aviso();
                                    avisoOLX.FHAlta = DateTime.Now;
                                    avisoOLX.Url = avisoUrlFiltradoSplit;
                                    avisoOLX.Insert();
                                    System.Diagnostics.Debug.WriteLine("Link procesado: " + avisoUrlFiltradoSplit);
                                }
                            }
                        }
                        indicePagina++;
                        System.Diagnostics.Debug.WriteLine("Indice sumarizado: " + indicePagina.ToString());
                        Thread.Sleep(5000);
                    }
                    catch (Exception ex)
                    {
                        detener = true;
                        System.Diagnostics.Debug.WriteLine("Aplicacion detenida. Error: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error intentando iniciar selenium: "+ex.Message);
                Console.Read();
            }

        }

        //BOT
        string url = "";
        ChromeOptions chromeOptions = new ChromeOptions();
        ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
        ChromeDriver driver = null;
        //OLX
        int indicePagina = 1;
        int cantidadCache = 0;
        string pLink = "https://www.olx.com.ar/autos-motos-y-barcos-cat-362-p-";
        bool detener = false;
        
    }
}
