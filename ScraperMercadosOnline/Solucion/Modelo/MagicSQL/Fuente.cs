﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace ScraperMercadosOnline
{
    public partial class Fuente : ISUD<Fuente>
    {
        public Fuente() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdFuente { get; set; }

        public string Nombre { get; set; }

        public string Url { get; set; }

        public DateTime? FHAlta { get; set; }

        public int? MaximoPaginas { get; set; }

        public DateTime? FHUltimaUtilizacion { get; set; }
    }
}