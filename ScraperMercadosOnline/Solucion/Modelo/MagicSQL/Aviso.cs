﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace ScraperMercadosOnline
{
    public partial class Aviso : ISUD<Aviso>
    {
        public Aviso() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdAviso { get; set; }

        public string Url { get; set; }

        public string Titulo { get; set; }

        public DateTime? FHAlta { get; set; }

        public int? IdFuente { get; set; }

        public string IdentificadorSerial { get; set; }

        public bool? Destacado { get; set; }

        public string Precio { get; set; }

        public string Lugar { get; set; }

        public bool? Negociable { get; set; }

        public string ImagenUrl { get; set; }

        public string Telefono { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }

        public string FechaPublicacion { get; set; }

        public string Moneda { get; set; }

        public string Usuario { get; set; }

        public bool? EsConcesionaria { get; set; }
    }
}