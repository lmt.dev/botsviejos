select 
	con.Celular,
	replace(avi.Lugar,'en ','') as 'Localidad',
	count(avi.IdAviso) 'Cantidad de avisos'
from ContactoConsecionaria con
INNER JOIN Aviso avi ON right(avi.Celular,10) = con.Celular
Where EsCliente is null
	and avi.Lugar not like '%Sastre%'
	and avi.Lugar not like '%Charata%'
	and con.EsBaja is null
	and con.ContactadoWhatsApp is null
group by con.Celular, avi.Lugar
having count(avi.IdAviso) > 1
order by avi.Lugar asc, 2 desc

select * from Aviso
--where 1 = 1
---- and EsConcesionaria = 1
--and Celular like '%3735538322'

update ContactoConsecionaria set ContactadoWhatsApp = 1 where Celular = '3416945295'

select * from Aviso where Celular is null